/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author marco
 */
public class FXMLCadastrarClienteController implements Initializable {

    @FXML
    private TextField tbxNome;
    @FXML
    private TextField tbxEmail;
    @FXML
    private TextField tbxCPF;
    @FXML
    private DatePicker datePiker;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    

    @FXML
    private void cadastraClick(ActionEvent event) throws IOException {
        Clientes n = new Clientes();
        n.setNome(tbxNome.getText());
        n.setEmail(tbxEmail.getText());
        n.setNascimento(Date.valueOf(datePiker.getValue()));
        n.setCpf(Long.parseLong(tbxCPF.getText()));
        n.insert();
        voltaMenu(null);
    }

    @FXML
    private void voltaMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLMenu.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
    }
    
}
