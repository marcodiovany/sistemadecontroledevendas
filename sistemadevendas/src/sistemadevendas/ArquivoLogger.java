/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import sistemadevendas.produtos.Venda;

/**
 *
 * @author Aluno
 */
public class ArquivoLogger {
    private File f;
    private BufferedWriter buff;

    public ArquivoLogger() {
        f = new File("default.txt");
        try
        {
            buff = new BufferedWriter(new FileWriter(f,true));
            buff.write("Arquivo Criado mas sem nome");
        }catch(IOException e)
        {
            System.out.println("Man voce se ferrou com o java !! ele ta te trolando!");
        }
    }
    public ArquivoLogger(String nome) {
        f = new File(nome);
        try
        {
            buff = new BufferedWriter(new FileWriter(f,true));
            System.out.println("Arquivo "+f.getName()+" Criado!");
        }catch(IOException e)
        {
            System.out.println("Man voce se ferrou com o java !! ele ta te trolando!");
        }
    }
    public void escrever(String linha)
    {
        try {
            buff.append(linha+"\r\n");
            buff.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            Logger.getLogger(ArquivoLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void escreverComData(String linha)
    {
        this.escrever(linha+" :"+new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
    }
    public void salvaVenda(Venda v) throws IOException
    {
        try {
            if(!f.exists())
            {
                buff = new BufferedWriter(new FileWriter(f,false));
                buff.write(0);
            }
            f.delete();
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream obs = new ObjectOutputStream(fos);
            obs.writeObject(v);
            obs.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArquivoLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public Venda carregaVenda() throws IOException, ClassNotFoundException
    {
        Venda v = null;
        if(!f.exists())
        {
            return null;
        }
        try {
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fis);
            v = (Venda) ois.readObject();
            ois.close();
            fis.close();
            f.delete();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArquivoLogger.class.getName()).log(Level.SEVERE, null, ex);
        }
        return v;
    }
    public void delFile()
    {
        boolean delete = f.delete();
    }
}
