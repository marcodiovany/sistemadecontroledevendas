/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas.JSON;

/**
 *
 * @author Aluno
 */
public class Cupom {
    private String codigo,formaDesconto;
    private int desconto;

    public Cupom() {
    }

    public Cupom(String Codigo) {
        this.codigo = Codigo;
    }

    public Cupom(String Codigo, String formaDesconto) {
        this.codigo = Codigo;
        this.formaDesconto = formaDesconto;
    }
    
    public Cupom(String Codigo, String formaDesconto, int desconto) {
        this.codigo = Codigo;
        this.formaDesconto = formaDesconto;
        this.desconto = desconto;
    }
    
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String Codigo) {
        this.codigo = Codigo;
    }

    public String getFormaDesconto() {
        return formaDesconto;
    }

    public void setFormaDesconto(String formaDesconto) {
        this.formaDesconto = formaDesconto;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }
    public String getFormaDescontoForUser() {
        if(formaDesconto.equals("porcentagem"))
        {
            return "Voce Ganhou "+desconto+" Porcentos de desconto!";
        }
        if(formaDesconto.equals("nItens"))
        {
            return "Voce Ganhou "+desconto+"Produtos de graça!";
        }
        if(formaDesconto.equals("maiorValor"))
        {
            return "O Item de MAIOR valor é de graça!";
        }
        if(formaDesconto.equals("menorValor"))
        {
            return "O Item de MENOR valor é de graça!";
        }
        if(formaDesconto.equals("valor"))
        {
            return "Voce Ganhou R$"+desconto+",00 de Desconto!";
        }
        return "Algo deu muito errado!";
    }
}
