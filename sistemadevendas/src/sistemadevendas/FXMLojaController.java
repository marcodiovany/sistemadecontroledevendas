/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

import com.google.gson.Gson;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.converter.LocalDateTimeStringConverter;
import jdk.internal.dynalink.support.NameCodec;
import sistemadevendas.JSON.Cupom;
import sistemadevendas.JSON.WebService;
import sistemadevendas.produtos.*;

/**
 * FXML Controller class
 *
 * @author marco
 */
public class FXMLojaController implements Initializable {

    @FXML
    private TableView<Produto> loja;
    @FXML
    private TableColumn<Produto, ?> lojaP;
    @FXML
    private TableColumn<Produto, ?> lojaD;
    @FXML
    private TableView<Produto> carrinho;
    @FXML
    private TableColumn<Produto, ?> carrinhoP;
    @FXML
    private TableColumn<Produto, ?> carrinhoD;
    @FXML
    private TableView<Clientes> cliente;
    @FXML
    private TableColumn<Clientes, ?> clienteNome;
    @FXML
    private TableColumn<Clientes, ?> clienteCPF;
    Produto lojaPselecao,carrinhoPselecao;
    Clientes selecaoCliente;
    Venda vendaArq;
    private Runnable gambiarra,salva,carrega,limpa;
    private Cupom nCupom;
    @FXML
    private Button bntArq;
    @FXML
    private Label labelCupom;
    @FXML
    private Label labelValor;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        loja.getItems().clear();
        carrinho.getItems().clear();
        cliente.getItems().clear();
        lojaP.setCellValueFactory(new PropertyValueFactory<>("nome"));
        lojaD.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        carrinhoP.setCellValueFactory(new PropertyValueFactory<>("nome"));
        carrinhoD.setCellValueFactory(new PropertyValueFactory<>("descricao"));
        clienteNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        clienteCPF.setCellValueFactory(new PropertyValueFactory<>("cpf"));
        cliente.getItems().addAll(Clientes.getAll());
        loja.getItems().addAll(Produto.getAll());
        salva = new Runnable() {
            @Override
            public void run() {
                
                ArquivoLogger arq = new ArquivoLogger("vendasave.bin");
                Venda nv = new Venda();
                if(selecaoCliente == null)
                {
                    return;
                }
                nv.setcliente(selecaoCliente);
                float valT=0;
                for(Produto p :carrinho.getItems())
                {
                    if(p != null)
                    {
                        VendaProduto vp = new VendaProduto();
                        valT += p.getPreco();
                        vp.setproduto(p);
                        vp.setQuantidade(1);
                        vp.setvenda(nv);
                        nv.addVendaProduto(vp);
                    }
                }
                nv.setValortotal(valT);
                nv.setData(java.sql.Date.valueOf(LocalDate.now()));
                initialize(null,null);
                try {
                    arq.salvaVenda(nv);
                } catch (IOException ex) {
                    Logger.getLogger(FXMLojaController.class.getName()).log(Level.SEVERE, null, ex);
                }
                bntArq.setText("Carregar");
                gambiarra = carrega;
            }
        };
        carrega = new Runnable() {
            @Override
            public void run() {
                loja.getItems().addAll(carrinho.getItems());
                carrinho.getItems().clear();
                
                cliente.setVisible(false);
                bntArq.setText("Limpar");
                gambiarra = limpa;
                if(vendaArq.getVendaProdutoSet().isEmpty())
                {
                    return;
                }
                for(VendaProduto vp:vendaArq.getVendaProdutoSet())
                {
                    carrinho.getItems().add(vp.getproduto());
                    if(loja.getItems().contains(vp.getproduto()))
                    {
                        loja.getItems().remove(vp.getproduto());
                    }
                }
            }
        };
        limpa = new Runnable() {
            @Override
            public void run() {
                vendaArq = null;
                cliente.setVisible(true);
                bntArq.setText("Salvar");
                gambiarra = salva;
                initialize(null,null);
            }
        };
        gambiarra = salva;
        
        ArquivoLogger Arq = new ArquivoLogger("vendasave.bin");
        try {
            vendaArq = Arq.carregaVenda();
            if(vendaArq != null)
            {
                bntArq.setText("Carregar");
                gambiarra = carrega;
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(FXMLojaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    

    @FXML
    @SuppressWarnings("UnnecessaryReturnStatement")
    private void finalizarCompraClick(ActionEvent event) {
        Venda nv = new Venda();
        if(selecaoCliente == null)
        {
            return;
        }
        nv.setcliente(selecaoCliente);
        float valT=0;
        Produto pDesc = carrinho.getItems().get(0);
        int cont = 0;
        for(Produto p :carrinho.getItems())
        {
            if(p != null)
            {
               
                VendaProduto vp = new VendaProduto();
                valT += p.getPreco();
                vp.setproduto(p);
                vp.setQuantidade(1);
                vp.setvenda(nv);
                nv.addVendaProduto(vp);
            }
        }
        nv.setValortotal(valT);
        nv.setData(java.sql.Date.valueOf(LocalDate.now()));
        WebService wc = new WebService();
        Gson g = new Gson();
        String vendaJ = g.toJson(carrinho.getItems());
        String result = null;
        try {
            result = wc.getContentAsString("http://peer.gq/webService/geraCupomDesconto?nome=MarcoDiovany&vendaJSON=itens:"+vendaJ);
        } catch (IOException ex) {
            Logger.getLogger(FXMLojaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        nCupom = g.fromJson(result, Cupom.class);
        labelCupom.setText("Cupom:"+nCupom.getFormaDescontoForUser());
        if(nCupom.getFormaDesconto().equals("porcentagem"))
        {
            valT = valT-(valT*(nCupom.getDesconto()/100));
        }else if(nCupom.getFormaDesconto().equals("valor"))
        {
            valT -= nCupom.getDesconto();
        }
        
        for(Produto p :carrinho.getItems())
        {
            if(p != null)
            {
                if(nCupom.getFormaDesconto().equals("menorValor") && pDesc.getPreco() > p.getPreco())
                {
                    pDesc = p;
                }
                if(nCupom.getFormaDesconto().equals("maiorValor") && pDesc.getPreco() < p.getPreco())
                {
                    pDesc = p;
                }
                if(nCupom.getFormaDesconto().equals("nItens") && cont < nCupom.getDesconto())
                {
                    carrinho.getItems().remove(p);
                    cont++;
                    nv.getVendaProdutoSet().retainAll(carrinho.getItems());
                }
            }
        }
        nv.insert();
        vendaJ = g.toJson(carrinho.getItems());
        try {
            wc.getContentAsString("http://peer.gq/webService/salvaVenda?nome=MarcoDiovany&venda={cupomDesconto:"+nCupom.getCodigo()+",itens:"+vendaJ+"} ");
        } catch (IOException ex) {
            Logger.getLogger(FXMLojaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArquivoLogger arq = new ArquivoLogger("vendaLog.log");
        arq.escreverComData(nv.toString());
        labelValor.setText("Valor Total = R$"+nv.getValortotal());
        initialize(null,null);
    }

    @FXML
    private void saveClick(ActionEvent event) throws IOException {
        gambiarra.run();
    }

    @FXML
    private void voltarClick(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLMenu.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
    }

    @FXML
    private void selecaoLoja(MouseEvent event) {
        lojaPselecao = loja.getSelectionModel().getSelectedItem();
        carrinho.getItems().add(lojaPselecao);
        loja.getItems().remove(lojaPselecao);
    }

    @FXML
    private void selecaoCarrinho(MouseEvent event) {
        carrinhoPselecao = carrinho.getSelectionModel().getSelectedItem();
        loja.getItems().add(carrinhoPselecao);
        carrinho.getItems().remove(carrinhoPselecao);
    }

    @FXML
    private void selecaoCliente(MouseEvent event) {
        selecaoCliente = cliente.getSelectionModel().getSelectedItem();
    }
    
}
