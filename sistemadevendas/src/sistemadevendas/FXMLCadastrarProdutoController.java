/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Aluno
 */
public class FXMLCadastrarProdutoController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    @FXML
    private TextField tbxNome;
    @FXML
    private TextField tbxValor;
    @FXML
    private TextField tbxDesc;
    @FXML
    private Label label1;
    @FXML
    private Button btnShowProdutos;
    @FXML
    private TableView<Produto> Tabela;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> precoT;
    @FXML
    private TableColumn<?, ?> descT;
    ArquivoLogger a;
    Produto selecao;
    @FXML
    private void handleButtonAction(ActionEvent event) {
        Produto np = new Produto();
        np.setNome(tbxNome.getText());
        np.setPreco(Float.parseFloat(tbxValor.getText()));
        np.setDescricao(tbxDesc.getText());
        Tabela.getItems().add(np);
        np.save();
        a.escreverComData(np.toString());
        tbxDesc.setText("");
        tbxNome.setText("");
        tbxValor.setText("");
        Tabela.refresh();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        a = new ArquivoLogger("cadastradosAqui.txt");
        Tabela.getItems().clear();
        Tabela.getItems().addAll(Produto.getAll());
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));
        precoT.setCellValueFactory(new PropertyValueFactory<>("preco"));
        descT.setCellValueFactory(new PropertyValueFactory<>("descricao"));
    }    

    @FXML
    private void deleteTabela(ActionEvent event) {
        Tabela.getItems().remove(selecao);
        selecao.delete();
        selecao = null;
        btnShowProdutos.setDisable(true);
    }

    @FXML
    private void tabelaMousePress(MouseEvent event) {
        selecao=Tabela.getSelectionModel().getSelectedItem();
        btnShowProdutos.setDisable(false);
    }

    @FXML
    private void voltarMenu(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLMenu.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
    }
    
}
