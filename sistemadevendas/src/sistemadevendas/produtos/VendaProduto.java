package sistemadevendas.produtos;

import conexao.Conexao;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import sistemadevendas.Produto;

/**
 * Model class of Venda_Produto.
 * 
 * @author generated by ERMaster
 * @version $Id$
 */
public class VendaProduto implements Serializable {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** Venda. */
	private Venda venda;

	/** Produto. */
	private Produto produto;

	/** Quantidade. */
	private Integer quantidade;

	/**
	 * Constructor.
	 */
	public VendaProduto() {
	}

	/**
	 * Set the Venda.
	 * 
	 * @param venda
	 *            Venda
	 */
	public void setvenda(Venda venda) {
		this.venda = venda;
	}

	/**
	 * Get the Venda.
	 * 
	 * @return Venda
	 */
	public Venda getvenda() {
		return this.venda;
	}

	/**
	 * Set the Produto.
	 * 
	 * @param produto
	 *            Produto
	 */
	public void setproduto(Produto produto) {
		this.produto = produto;
	}

	/**
	 * Get the Produto.
	 * 
	 * @return Produto
	 */
	public Produto getproduto() {
		return this.produto;
	}

	/**
	 * Set the Quantidade.
	 * 
	 * @param quantidade
	 *            Quantidade
	 */
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * Get the Quantidade.
	 * 
	 * @return Quantidade
	 */
	public Integer getQuantidade() {
		return this.quantidade;
	}
        public void insert()
        {
            Conexao c = new Conexao();
            Connection conexao = c.getConexao();
            PreparedStatement ps,pss;
            try
            {
                pss = conexao.prepareStatement("select * from venda order by id desc");
                pss.executeUpdate();
                ResultSet rs = pss.getResultSet();
                ps = conexao.prepareStatement("insert into venda_produto values (?,?,?)");
                rs.next();
                ps.setInt(1,rs.getInt("id"));
                ps.setInt(2,this.produto.getId());
                ps.setInt(3,this.quantidade);
                ps.executeUpdate();
            }catch(SQLException e)
            {
                e.printStackTrace();
            }
        }

    @Override
    public String toString() {
        return produto.toString() + ", quantidade=" + quantidade;
    }
        
}
