/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLMenuController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void toLoja(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLoja.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
    }

    @FXML
    private void toCadastrarProduto(ActionEvent event) throws IOException {
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLCadastrarProduto.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
        
    }

    @FXML
    private void toCadastrarCliente(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLCadastrarCliente.fxml"));
        Scene scene = new Scene(root);
        sistemadevendas.Sistemadevendas.tela.setScene(scene);
    }
    
}
